<?php

/**
 * @file
 * Contains Drupal Commerce specific hook implementations.
 */


/**
 * Implements hook_commerce_product_type_info_alter().
 */
function commerce_custom_product_line_item_commerce_product_type_info_alter(&$types) {
  foreach ($types as $name => &$type) {
    // @todo: investigate how to handle settings specified in code and how to
    // handle the ability to lock those.

    //$type += array('line_item_type_settings_locked' => FALSE);

    //$instance = field_info_instance('commerce_product', 'commerce_line_item_type', $name);
    //$type['use_line_item_type'] = !!$instance;

    //// If an instance exists, merge its settings.
    //if ($instance) {
      //$type['line_item_type_settings'] = commerce_custom_product_line_item_field_instance_line_item_settings($instance);
      //$type['use_line_item_type'] = TRUE;
    //}
    //// If the product type indicates it wants to use line item type settings.
    //elseif (!empty($type['use_line_item_type'])) {
      //// If the settings were specified, merge in any missing defaults.
      //if (isset($type['line_item_type_settings']) && is_array($type['line_item_type_settings'])) {
        //$type['line_item_type_settings'] += commerce_custom_product_line_item_type_default_settings();
      //}
      //// Otherwise, just use the defaults.
      //else {
        //$type['line_item_type_settings'] = commerce_custom_product_line_item_type_default_settings();
      //}
    //}
    //// Otherwise this product type does not use line item types.
    //else {
      //$type['line_item_type_settings'] = FALSE;
      //$type['use_line_item_type'] = FALSE;
    //}
  }
}
