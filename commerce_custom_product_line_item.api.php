<?php

/**
 * @file
 * Hooks provided by the Customizable Products - Line Item module.
 */

/**
 * Allows modules to alter the url used for editing line items on product pages.
 *
 * @params &$url
 *   Array consisting of:
 *   - path: path string as used by url()
 *   - options: options array as used by url()
 *
 * @see url()
 */
function hook_commerce_custom_product_line_item_commerce_display_path_edit_alter(&$url, $line_item) {
  // No example.
}
