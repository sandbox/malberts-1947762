Commerce Customizable Products - Line Item Type
===============================================

By default there is no relationship between a product and the line item type
that will be used to represent it. This modules alters this by allowing a
product to define an associated line item type that will get used on Add to Cart
forms as well as during price calculations.

Setup
-----

 - Edit the product type at admin/commerce/products/types.
 - Mark the "Use custom line item type" checkbox. This will add the line item
   type field instance to this product type.
 - Open the "Manage Fields" tab.
 - Edit the "Line item type" field.
 - Change settings under "Line item type settings".

To disable line item association, either remove the field or edit the product
type and unmark the checkbox.

What happens
------------

 - Add to Cart forms now use the associated line item type for the selected
   product.
 - Sell price calculation will pass in the associated line item type.
 - Products not using this functionality will continue to work as before.
 - Different line item types are possible on a single Add to Cart form by
   by rebuilding the line item fields during the AJAX attributes refresh.
