<?php

/**
 * @file
 * Provides metadata for the product entity.
 */


/**
 * Implements hook_entity_property_info_alter() on top of the Product module.
 */
function commerce_custom_product_line_item_entity_property_info_alter(&$info) {
  // Provide a property to make accessing the line item type easier. This is
  // not the commerce_line_item_type field, but rather a wrapper around that.
  $info['commerce_product']['properties']['line_item_type'] = array(
    'label' => t('Line item type'),
    'description' => t('The line item type associated with this product, if applicable.'),
    'type' => 'token',
    'getter callback' => 'commerce_custom_product_line_item_commerce_product_get_properties',
    'calculated' => TRUE,
  );

  // Property to get a URL which includes the parameter needed to edit a line
  // item on the display path.
  $info['commerce_line_item']['properties']['commerce_display_path_edit'] = array(
    'label' => t('Display path (edit line item)'),
    'description' => t('The display path with a parameter to edit the line item.'),
    'type' => 'text',
    'getter callback' => 'commerce_custom_product_line_item_commerce_line_item_get_properties',
    'calculated' => TRUE,
    'entity views field' => TRUE,
  );
}
